\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {paragraph}{}{1}{section*.2}
\contentsline {paragraph}{}{1}{section*.3}
\contentsline {paragraph}{}{1}{section*.4}
\contentsline {paragraph}{}{1}{section*.5}
\contentsline {section}{\numberline {2}Functional Magnetic Materials \& Their Applications}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Advances in Magnetostrictive Materials}{1}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Advances in Magnetocaloric Materials}{1}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Applications}{1}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Damage Sensors}{1}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Commercial Refrigeration}{1}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Toward an Experimental Design Approach for Magnetocaloric Refrigeration Systems \cite {bernal2016toward}}{1}{section.3}
\contentsline {section}{\numberline {4}Inverse-magnetostriction-induced switching current reduction of STT-MTJs and its application for low-voltage MRAM \cite {takamura2017inverse}}{1}{section.4}
\contentsline {subsection}{\numberline {4.1}The Search for ``Universal Memory''}{1}{subsection.4.1}
\contentsline {paragraph}{}{1}{section*.7}
\contentsline {paragraph}{}{1}{section*.8}
\contentsline {paragraph}{}{2}{section*.9}
\contentsline {section}{\numberline {5}Comparison}{2}{section.5}
\contentsline {section}{\numberline {6}Conclusion}{2}{section.6}
\contentsline {section}{\numberline {7}Appendix}{i}{section.7}
\contentsline {section}{List of Figures}{i}{section.7}
\contentsline {section}{List of Tables}{i}{section*.10}
\contentsline {section}{References}{i}{section*.11}
