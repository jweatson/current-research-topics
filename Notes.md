#J1649+2635: A Grand-Design Spiral with a Large Double-Lobed Radio Source
##The paper that started it all

- Discovery of a grand-design spiral galaxy, double-lobed radio source
	- This goes against the known models of spiral galaxy evolution
	- Galactic radio sources typically require a large amount of input gas, typically accomplished through a recent merger.

>can mergers form into spirals, or is there something else at play?

- $z=0.0545$, quite nearby
	- $96kpc$ diffuse optical halo, extremely large, implies massive particle jets.
	- $M_{dyn}=7.7\times 10^{13}M_\odot$
- Source may be similar to a Seyfert galaxy, located in a denser than normal environment.
- These may be more prevalent in the high $z$ universe

>Probably not going to be detected with current telescope technology.

##Introduction
- It is postulated that elliptical galaxies form from spiral mergers
	- Merger of 2 gas-rich galaxies creates conditions for star formation
	- In addition, sudden influx of material into central SMBH leads to the initiation of an AGN phase
	- Starburst winds deplete reserves of the galaxy, leading to dust-poor conidtions of an evolved elliptical galaxy
		- Although there are starbust and quasar driven winds, quasar winds tnd to occur at a later age
	- Its not uncommon for spiral galaxies to have an AGN phase, but these are typically weak

> Due to low inflow rate? Black hole mass?

`Kinney et al. 2000, Schmitt et al. 2001, Momjian et al. 2003`
>**Accretion rate apparently!**

- A merger may be required to launch kpc scale jets
	- Process theorised to account for double lobed radio sources, which should inevitably form an elliptical, a gas rich spiral should not produce such powerful jets

##Radio Galaxies That are Hosted by Spirals

- Powerful radio sources have been detected from spirals, but they were typically detected from lenticular or disk-dominated (no large bulge) galaxies

>The galaxy discovered in this paper is a **grand design** spiral

- Other spiral galxies have interesting features
	- 0313-192 is an edge on spiral, hosting a large double lobed radio source
		- Over luminous bulge implies large black hole
		- Projected orientation of lobes is perpendicular to the disk, implies less dense ISM?
		- Warped stellar disk implies minor merger occuring in the past
	- Speca, optically red but with signs of star formation
		- Episodic radio emission, changing flux throughout the structure?
		- Relic radio lobes, $1.3Mpc$ out host radio source may be a giant radio galaxy
		- Speca may be accreting cold gas from the ISM, which removes the need for a merger
	- J2345-0449 Appears to be in isolated environment, $1.63Mpc$ radio emission, authors suggest that a merger is not required, fast-spinning black hole and relatively inefficient accretion.

`Bagchi et al. 2014`

###J1649+2635

- Systematic search for spiral galaxies undertaken
	- Radio Galaxy zoo
	- 3 sources identified as spiral galaxies in the superclean sample
		- Chance alignment
		- Nearby massive star forming galaxy
		- J1649+2635
- Spiral morphology identified before radio lobes discovered
- g-band magnitude of 15.50
- Radio source identified in NVSS and FIRST surveys
	- Clearly shows double structure in FIRST, but some large scale emission is detected in NVSS
- Very luminous bulge
- Evidence for a bar structure
- Spectra only available for the bulge, due to size of optical fibres
- Galaxy is red in colour, implies lack of recent star formation
	- This subclass of red spirals first discovered by galaxy zoo
	- 3x less star formation in red spirals, cool gas flow cut off by some mechanism, such as a merger
- Extent of radio source comparible in scale to a galactic halo
- Star formation rate $1.89M_\odot/year$

>Is that a lot? You should check

- Black hole mass $3.7\times 10^8 M_\odot$ similar in size to ones that hold extremely powerful radio emissions, much larger than a typical spiral galaxies black hole
	- Radio galaxies empirically require large black holes

>Possibly the result of a merger?

- Yes, as black hole also requires large rotational velocity

###The Environment

- Poor cluster, dynamical mass of $M=6.6\pm 1.7 \times 10^{13} M_\odot$ 
- One of the two brightest galaxies in the system
- More like a group of galaxies, than a cluster dynamically

###Possible Causes

- J1649+2635 is a hybrid, may be more prevalent in the early universe (*Mao 2010*), may provide an excellent probe for early universe behavior
- Undergone a planar minor merger, which has no so far disrupted the disk, nor will it disrupt too much
	- Maybe the process that quenches star formation period
- J1649+2635 may be a normal galaxy that has undergone a merger, that has caused it to grow spiral arms, that may dissipate rapidly
- Seyfert galaxy in a dense environment?
	- Could the IGN support dense radio lobes?
- Further observations are required to test these hypotheses

#Data Driven Dissection of Emission Line Regions in Seyfert Galaxies

