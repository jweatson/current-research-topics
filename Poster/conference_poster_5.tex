%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% a0poster Landscape Poster
% LaTeX Template
% Version 1.0 (22/06/13)
%
% The a0poster class was created by:
% Gerlinde Kettl and Matthias Weiser (tex@kettl.de)
% 
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[a0,landscape]{a0poster}
%\usepackage{parskip}
\usepackage{float}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{multicol} % This is so we can have multiple columns of text side-by-side
\columnsep=100pt % This is the amount of white space between the columns in the poster
\columnseprule=3pt % This is the thickness of the black line between the columns in the poster
\usepackage[font=Large,justification=centering]{caption}
\usepackage[svgnames]{xcolor} % Specify colors by their 'svgnames', for a full list of all colors available see here: http://www.latextemplates.com/svgnames-colors

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{pdfpages}
\usepackage{url}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{float}

\usepackage{color}
\usepackage{framed}
\usepackage{grffile}
\usepackage{wrapfig}
\usepackage{longtable}

\let\oldfootnotesize\footnotesize
\renewcommand*{\footnotesize}{\oldfootnotesize\normalsize}

\usepackage{graphicx} % Required for including images
\graphicspath{{figures/}} % Location of the graphics files
\usepackage{booktabs} % Top and bottom rules for table
\usepackage[font=normalsize,labelfont=bf]{caption} % Required for specifying captions to tables and figures
\usepackage{amsfonts, amsmath, amsthm, amssymb} % For math fonts, symbols and environments
\usepackage{wrapfig} % Allows wrapping text around tables and figures

\bibliographystyle{apalike}

\hypersetup{
	colorlinks=true,
	linkcolor=NavyBlue,
	filecolor=magenta,
	urlcolor=red,
	citecolor=red,
}

\begin{document}

%----------------------------------------------------------------------------------------
%	POSTER HEADER 
%----------------------------------------------------------------------------------------

% The header is divided into three boxes:
% The first is 55% wide and houses the title, subtitle, names and university/organization
% The second is 25% wide and houses contact information
% The third is 19% wide and houses a logo for your university/organization or a photo of you
% The widths of these boxes can be easily edited to accommodate your content as you see fit
\colorbox{NavyBlue!80}{
\begin{minipage}[b]{0.79\linewidth}
\vspace{1cm} \veryHuge \color{white} \textbf{\textit{DRAGNquest: The Search for Kiloparsec Scale Radio Emission Produced by Spiral Galaxies}} \color{Black}\\ % Title
% Subtitle
\Large \color{white} \textbf{Joseph Eatson} \\ % Author(s)
\Large \color{white} \textbf{Current Research Topics in Physics}\\ % University/organization
\end{minipage}
%
\begin{minipage}[b]{0.0001\linewidth}
	
\end{minipage}
%
\begin{minipage}[b]{0.2\linewidth}
	\includegraphics[width=20cm]{logo.png} % Logo or a photo of you, adjust its dimensions here
	\vspace{1cm}
\end{minipage}
}

\vspace{3cm}

\begin{multicols}{4}

{\large
\section*{Terminology}
\begin{itemize}
	\item \textbf{\textit{SMBH}} (SuperMassive Black Hole) a term used to describe black holes over $10^6M_\odot$
	\item \textbf{\textit{AGN}} (Active Galactic Nuclei) occur when the SMBH at the centre of a galaxy is currently emitting radiation due to the absorption of infalling matter.
	\item \textbf{\textit{DRAGN}} (Double lobed Radio Active Galactic Nuclei) Comprehensive term for any AGN producing powerful relativistic jets, which emit synchrotron radiation on a kiloparsec scale.
	\item \textbf{\textit{Fanaroff-Riley Classification}} Sub-categorises two main DRAGN types:
	\begin{itemize}
		\item \textbf{\textit{Class I}} objects have jets whose luminosity \textit{decreases} with distance from the nucleus.
		\item \textbf{\textit{Class II}} objects have jets whose luminosity \textit{increases} with distance from the nucleus.
	\end{itemize}
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{../DRAGN/Images/DRAGn}
	\caption{An example of FR-II emission, showing greater radio brightness in the lobes}
	\label{fig:fanaroff4}
\end{figure}

\begin{itemize}
	\item \textbf{\textit{Synchrotron Radiation}} refers to electromagnetic radiation emitted when a charged particle travels on a curved path at relativistic speeds due to a magnetic field.
	\item \textbf{\textit{VLBI}} (Very Long Baseline Interferometry) is the use of multiple radio telescopes spaced thousands of kilometres apart to create a large synthetic aperture with a very fine angular resolution.
\end{itemize}

\section*{Introduction}
\begin{itemize}
 	\item Galaxies containing DRAGNs were assumed to be only elliptical.
 	\item However astronomers such as Dr. Minnie Mao have found spiral galaxies with kpc scale lobes and radio emission on par with elliptical DRAGNs\footnote{ Mao et al. (2015). J1649+2635: a Grand-Design Spiral With a Large Double-Lobed Radio Source. Monthly Notices of the Royal Astronomical Society, 446(4), 4176-4185.}.
 	\item First galaxy discovered was 0313-192, an FR-I DRAGN\footnote{ Keel et al. The Spiral Host Galaxy of the Double Radio Source 0313− 192. The Astronomical Journal, 132(6), 2233.}.
 \end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../DRAGN/Images/0313crop}
	\caption{An overlay of radio data on an optical image of 0313-192 showing radio lobes significantly larger than the galactic disk, as well as a large central bulge}
	\label{fig:0313}
\end{figure}

\section*{The DRAGN That Broke All The Rules}
\begin{itemize}
	\item SMBH are theorised to produce jets through relativistic frame dragging (Blandford-Znajek or Penrose processes), the more accretion that occurs, the stronger the jet.
	\item AGN do exist in spiral galaxies, but typically there is a lack of material falling into the galaxy, and the jets produced are typically on the scale of a few parsecs (traditional Seyfert galaxies).
	\item In order to break through the disk of the galaxy, enormous amounts of energy is required.
	\item This source of matter was theorised to be a galactic merger, but these destroy the spiral structure of the galaxy by disrupting the disk.	
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.9\linewidth]{"figures/Tadpole Galaxy"}
\caption{The Tadpole Galaxy, UGC 10214, an example of a disrupted spiral galaxy, due to a merger occuring 100 million years ago}
\label{fig:TadpoleGalaxy}
\end{figure}

\section*{DRAGNheart}

\begin{itemize}
	\item Using VLBI, Mao's team analysed spiral galaxies containing DRAGNs with resolution sufficient to observe the central bulge.
	\item A link between all spiral DRAGN galaxies is that the central bulge is extremely large, suggesting a black hole in the range of $10^8M_\odot$.
	\item Spiral DRAGN galaxies are also reddened, suggesting that there has not been a recent starburst phase and that any merger was long enough ago that disruption would be noticeable.
\end{itemize}

\section*{Big Trouble With Little Mergers}
\begin{itemize}
	\item It has been proposed that a planar merger is the matter source for the jet\footnote{ Singh et al. (2015). Discovery of Rare Double-Lobe Radio Galaxies Hosted in Spiral Galaxies. Monthly Notices of the Royal Astronomical Society, 454(2), 1556-1572.}.
	\item Spiral DRAGNs are extremely rare, suggesting that a lot of specific conditions have to be met.
	\item Mao also hypothesises that a denser intergalactic medium may promote the growth of kpc scale jets, and spiral DRAGN galaxies may be closely related to Seyfert galaxies.
	\item Dependence on region density may also mean that spiral DRAGNs were more common in the earlier universe.
\end{itemize}
\section*{For a Few DRAGNs More - Radio Galaxy Zoo}

\begin{itemize}
	\item Galaxy Zoo was a citizen science project that aimed to classify galaxies in the Sloan Digital Sky Survey\footnote{ Willett, K. W. (2016). Radio Galaxy Zoo: Host Galaxies and Radio Morphologies for Large Surveys From Visual Inspection. arXiv preprint arXiv:1603.02645.}. It was used by Mao to discover J1649+2635.
	\item Radio Galaxy Zoo is an offshoot of this project, and aims to classify radio galaxies by overlaying SDSS survey image data with radio data from the VLA FIRST survey.
\end{itemize}

\begin{figure}[H]
\centering
\includegraphics[width=0.9\linewidth]{../DRAGN/Images/RGZ}
\caption{An example of the Radio Galaxy Zoo interface}
\label{fig:RGZ}
\end{figure}

\begin{itemize}
	\item Mao hopes that RGZ could be used to detect more spiral DRAGNs.
	\item RGZ has resulted in the detection of hybrid galaxies, HyMORS, where one lobe is characteristic of FR-I emission but with FR-II lobe brightening in the other. This implies that there is still much we do not understand about radio galaxies.
\end{itemize}

}


\end{multicols}

\end{document}