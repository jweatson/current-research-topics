\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {paragraph}{}{1}{section*.2}
\contentsline {paragraph}{}{1}{section*.3}
\contentsline {paragraph}{}{1}{section*.4}
\contentsline {paragraph}{}{1}{section*.5}
\contentsline {section}{\numberline {2}Spiral DRAGNs}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}Background}{1}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Radio Loud Active Galactic Nuclei}{1}{subsubsection.2.1.1}
\contentsline {paragraph}{}{1}{section*.6}
\contentsline {paragraph}{}{1}{section*.7}
\contentsline {paragraph}{}{1}{section*.8}
\contentsline {paragraph}{}{1}{section*.9}
\contentsline {paragraph}{}{2}{section*.10}
\contentsline {paragraph}{}{2}{section*.11}
\contentsline {paragraph}{}{2}{section*.12}
\contentsline {subsubsection}{\numberline {2.1.2}Spiral DRAGNs - Breaking the Accepted Models}{2}{subsubsection.2.1.2}
\contentsline {paragraph}{}{2}{section*.13}
\contentsline {paragraph}{}{2}{section*.14}
\contentsline {paragraph}{}{2}{section*.15}
\contentsline {paragraph}{}{2}{section*.16}
\contentsline {subsubsection}{\numberline {2.1.3}The Importance of Spiral DRAGNs}{2}{subsubsection.2.1.3}
\contentsline {paragraph}{}{2}{section*.17}
\contentsline {subsection}{\numberline {2.2}Here Be DRAGNs}{2}{subsection.2.2}
\contentsline {paragraph}{}{2}{section*.18}
\contentsline {paragraph}{}{3}{section*.19}
\contentsline {subsubsection}{\numberline {2.2.1}Discovery of Spiral DRAGNs}{3}{subsubsection.2.2.1}
\contentsline {paragraph}{}{3}{section*.20}
\contentsline {paragraph}{}{3}{section*.21}
\contentsline {subsubsection}{\numberline {2.2.2}Properties of J1649+26 }{3}{subsubsection.2.2.2}
\contentsline {paragraph}{}{3}{section*.22}
\contentsline {paragraph}{}{3}{section*.23}
\contentsline {subsubsection}{\numberline {2.2.3}Potential Explanation of Phenomena}{3}{subsubsection.2.2.3}
\contentsline {paragraph}{}{3}{section*.24}
\contentsline {paragraph}{}{3}{section*.25}
\contentsline {paragraph}{}{3}{section*.26}
\contentsline {subsubsection}{\numberline {2.2.4}The Heart of the DRAGN - Use of VLBI}{3}{subsubsection.2.2.4}
\contentsline {paragraph}{}{4}{section*.27}
\contentsline {section}{\numberline {3}Discovery of Rare Double-Lobe Radio Galaxies Hosted in Spiral Galaxies \cite {singh2015discovery} - A Summary}{4}{section.3}
\contentsline {paragraph}{}{4}{section*.28}
\contentsline {subsection}{\numberline {3.1}Introduction}{4}{subsection.3.1}
\contentsline {paragraph}{}{4}{section*.29}
\contentsline {subsection}{\numberline {3.2}Observations}{4}{subsection.3.2}
\contentsline {paragraph}{}{4}{section*.30}
\contentsline {paragraph}{}{4}{section*.31}
\contentsline {paragraph}{}{4}{section*.32}
\contentsline {paragraph}{}{5}{section*.33}
\contentsline {subsection}{\numberline {3.3}Results and Discussion}{5}{subsection.3.3}
\contentsline {paragraph}{}{5}{section*.34}
\contentsline {paragraph}{}{5}{section*.35}
\contentsline {paragraph}{}{5}{section*.36}
\contentsline {paragraph}{}{5}{section*.37}
\contentsline {paragraph}{}{5}{section*.38}
\contentsline {paragraph}{}{5}{section*.39}
\contentsline {section}{\numberline {4}Radio Galaxy Zoo: Host Galaxies and Radio Morphologies For Large Surveys From Visual Inspection \cite {willett2016radio} - A Summary}{5}{section.4}
\contentsline {paragraph}{}{5}{section*.40}
\contentsline {paragraph}{}{6}{section*.41}
\contentsline {subsection}{\numberline {4.1}Introduction}{6}{subsection.4.1}
\contentsline {paragraph}{}{6}{section*.42}
\contentsline {paragraph}{}{6}{section*.43}
\contentsline {paragraph}{}{6}{section*.44}
\contentsline {paragraph}{}{6}{section*.45}
\contentsline {paragraph}{}{6}{section*.46}
\contentsline {subsection}{\numberline {4.2}Data, Website \& Interface}{6}{subsection.4.2}
\contentsline {paragraph}{}{6}{section*.47}
\contentsline {paragraph}{}{6}{section*.48}
\contentsline {paragraph}{}{7}{section*.49}
\contentsline {paragraph}{}{7}{section*.50}
\contentsline {subsection}{\numberline {4.3}Results and Discussion}{7}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Comparison to WISE}{7}{subsubsection.4.3.1}
\contentsline {paragraph}{}{7}{section*.51}
\contentsline {subsubsection}{\numberline {4.3.2}HyMORS}{7}{subsubsection.4.3.2}
\contentsline {paragraph}{}{7}{section*.52}
\contentsline {paragraph}{}{7}{section*.53}
\contentsline {subsubsection}{\numberline {4.3.3}Bending Angles}{7}{subsubsection.4.3.3}
\contentsline {paragraph}{}{7}{section*.54}
\contentsline {paragraph}{}{7}{section*.55}
\contentsline {paragraph}{}{7}{section*.56}
\contentsline {section}{\numberline {5}Conclusion}{8}{section.5}
\contentsline {subsection}{\numberline {5.1}Mao et. al's Work}{8}{subsection.5.1}
\contentsline {paragraph}{}{8}{section*.57}
\contentsline {paragraph}{}{8}{section*.58}
\contentsline {subsection}{\numberline {5.2}Singh et. al's Work}{8}{subsection.5.2}
\contentsline {paragraph}{}{8}{section*.59}
\contentsline {paragraph}{}{8}{section*.60}
\contentsline {subsection}{\numberline {5.3}Willet et. al's Work}{8}{subsection.5.3}
\contentsline {paragraph}{}{8}{section*.61}
\contentsline {paragraph}{}{8}{section*.62}
\contentsline {section}{\numberline {6}Appendix}{i}{section.6}
\contentsline {subsection}{\numberline {6.1}List of Discovered Spiral DRAGNs}{i}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Synthetic Aperture Generation \& Discussion}{i}{subsection.6.2}
\contentsline {section}{\numberline {7}Lists \& References}{i}{section.7}
