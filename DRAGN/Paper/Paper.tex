\documentclass[10pt,a4paper]{article}
\usepackage[inner=3cm, outer=3cm, top=3cm, bottom=3cm]{geometry}
\usepackage[latin1]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{cite}
\usepackage{pdfpages}
\usepackage{url}
\usepackage{multicol}
\usepackage{graphicx}
\usepackage{cite}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{float}
\usepackage[justification=centering,labelfont=it,textfont=it]{caption}
\usepackage{color}
\usepackage{framed}
\usepackage{grffile}
\usepackage{wrapfig}
\usepackage{longtable}

\newcommand{\maogal}{J1649+26 }
\bibliographystyle{apalike}

\hypersetup{
    colorlinks=true,
    linkcolor=red,
    filecolor=magenta,
    urlcolor=red,
    citecolor=red,
}
%opening
\title{Current Research Topics in Physics \\ DRAGN Quest: The Search For Powerful Spiral Radio Galaxies}
\author{Joseph Eatson}
\date{December 2016}

\begin{document}

\begin{titlepage}
\maketitle
\vspace{1cm}

\begin{figure}[H]
\centering
\includegraphics[width=0.6\linewidth]{../Images/DRAGNquest}
\end{figure}

\pagenumbering{gobble}
\end{titlepage}
\tableofcontents
\pagenumbering{gobble}
\newpage
\pagenumbering{arabic}% Arabic page numbers (and reset to 1)

\begin{multicols}{2}
\section{Introduction}
\paragraph{}
Galaxies emitting powerful jets of radiation have been initially assumed to be exclusively elliptical. However recent chance observations, and a full scale survey using citizen science resources have found exceedingly rare spiral galaxies that possess powerful double lobed radio emissions, known as spiral DRAGNs (Double lobed Radio Active Galactic Nuclei).

\paragraph{}
These spiral galaxies with strong radio emissions defy the currently understood models of galactic evolution, as the central black hole of a galaxy requires a large mass accretion rate in order to maintain a high powered relativistic jet on a kiloparsec scale; this tremendous amount of material can only be acquired via a galactic merger, which destroys the spiral structure of both galaxies, forming a larger elliptical galaxy.

\paragraph{}
So far, the number of spiral DRAGNs discovered is in the single digits, and have not been detected nearby, however they may hold the key for early evolution of galaxies, and may be very populous in the high-redshift early universe.

\paragraph{}
This paper will cover the talk \textit{Here Be DRAGNs} by Dr. Minnie Mao of the University of Manchester, as well as in depth reviews of related papers, \textit{Discovery of Rare Double-Lobe Radio Galaxies Hosted in Spiral Galaxies} \cite{singh2015discovery} and \textit{Radio Galaxy Zoo: Host Galaxies and Radio Morphologies For Large Surveys From Visual Inspection} \cite{willett2016radio}.


\section{Spiral DRAGNs}

\subsection{Background}
\subsubsection{Radio Loud Active Galactic Nuclei}

\paragraph{}
As matter falls into (accretes) the central supermassive black hole (SMBH) of a galaxy, twin jets of charged particles are emitted, travelling perpendicular to the plane of the galaxy. The exact process for this phenomena isn't fully understood, but it is theorised that particles near the event horizon extract energy from the rapidly spinning, Kerr-type black hole; the leading processes behind this being the Blandford-Znajek process \cite{blandford1977electromagnetic} and the Penrose process. The Blandford-Znajek process is well accepted \cite{koide2002extraction}, and describes how a magnetic field interacts with in-falling charged particles, forcing these particles to extract energy out of the spinning black hole, rapidly accelerating them to near the speed of light, while the Penrose process involves a general relativistic phenomenon called frame dragging to extract this energy\cite{penrose1971extraction}. Another set of theories involve magnetically accelerated jets, but there is some debate whether these are correct, as black hole energy extraction mechanisms applied to relativistic jets typically operate under the assumption that SMBHs rotate \cite{rosswog2007introduction}.

\paragraph{}
The amount of energy imparted to the relativistic jet is largely dependent on the black hole mass and in-fall rate onto the SMBH \cite{longair2011high}, meaning that for the largest jets require extremely large amounts of in-falling matter, much larger than intra-galactic molecular clouds that drive other phenomena, such as quasars (which are radio loud, but more compact) and Seyfert galaxies (which are radio quiet, and with emissions typically confined to 1 parsec).

\paragraph{}
The relativistic jet becomes radio loud due to interaction with the intergalactic medium, emitting radiation through the synchrotron process as well as inverse compton scattering with the cosmic microwave background, which produces a non-thermal, polarised emission spectrum \cite{longair2011high}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.8\linewidth]{../Images/cygA}
	\caption{Synchrotron spectrum of the galaxies Cas-A and Cyg-A, showing flat spectrum \cite{baars1977absolute}}
	\label{fig:cyga}
\end{figure}

\paragraph{}
A flat spectrum such as this is another property that separates a radio galaxy from other types of galactic nuclei; quasars also have a synchrotron spectrum, but emit large amounts of visible light as well.


\paragraph{}
Radio galaxies can be further subdivided into the Fanaroff-Riley classification system. Class I refers to sources whose radio luminosity tapers off as distance to the nucleus increases, while class II classifies radio galaxies with increasing luminosity at \textit{increasing} distance. This implies that two distinct types of interaction are occurring between the relativistic jet and the intergalactic medium. In addition, class II radio galaxies have significantly higher luminosities, with faint jets leading into the extremely bright lobes.

\begin{figure}[H]
\centering
\includegraphics[width=0.7\linewidth]{../Images/fanaroff4}
\caption{A VLA map of the FR-II quasar 3C 47, \cite{bridle1994deep}}
\label{fig:fanrff4}
\end{figure}

\paragraph{}
Figure \ref{fig:fanrff4} shows two other distinct properties of a FR-II galaxy; that the lobes are quite asymmetric, and that there is little interaction with the IGM and the relativistic jets until reaching the lobes.


\paragraph{}
It is well understood that in order to generate kiloparsec scale radio jets, the central supermassive black hole must have an extremely large rate of in-falling material \cite{longair2011high}, which can only result from a merger of two spiral galaxies. There is a strict delimitation between kiloparsec and parsec scale jets as the turbulent environment of the galaxy prevents lower energy jets from escaping; an extremely large amount of energy is required to break free from the structure of the galaxy \cite{ledlow1998unusual}.

\subsubsection{Spiral DRAGNs - Breaking the Accepted Models}

\paragraph{}
The first discovered spiral DRAGN was announced in a report by \cite{ledlow1998unusual}, describing a FR I radio source in a flattened disk-dominated galaxy. The galaxy was observed in radio, optical, and IR to determine that this extremely luminous radio emission was not the product of rapid star formation; this was further confirmed by the detection of symmetric double radio lobes extending $\sim 200\text{ kpc}$ from the spiral arms.

\paragraph{}
Double lobed radio galaxies have been observed to have a flattened, spiral structure, but are typically remnants of a recent merger, as the spiral arms are highly warped, indicating that they will shortly dissipate. Spiral DRAGNs are noted to be highly red, and have an average rate of star formation \cite{mao2015j1649+}; this implies that spiral DRAGNs have not undergone a significant starburst period, and that they are old enough that any disruption of their spiral structure should be readily apparent.

\paragraph{}
There are some additional curiosities to spiral DRAGNs; \cite{singh2015discovery} observes multiple spiral DRAGNs, and notes that while the morphology or the radio lobe is similar to a FR-II class radio galaxy, the $1.4\text{GHz}$ radio luminosity is in the range $10^{24}$-$10^{25}$ W Hz$^{-1}$. Singh also notes the extreme size of the galactic bulge in spiral DRAGNs, with an extremely large SMBH of mass $10^8 \text{M}_\odot$; this is corroborated by \cite{johnson2015unusual}, who notes it in the discovery of another spiral DRAGN.

\paragraph{}
Currently discovered spiral DRAGNs, according to \cite{mao2015j1649+}, can be found in the appendix, section \ref{listDRAGN}.


\subsubsection{The Importance of Spiral DRAGNs}
\paragraph{}
Spiral DRAGNs are an important feature of galactic evolution, as they are theorised to be extremely prevalent in the early universe \cite{mao2015j1649+}. This was because the universe was denser, with a greater number of galaxy mergers and a wider distribution of galaxy masses as well as a greater average IGM density. This theoretically decreased the amount of in-fall mass required for the creation of radio lobes.


\subsection{Here Be DRAGNs}

\paragraph{}
The talk delivered by Dr. Minnie Mao, \textit{Here be DRAGNs}, was regarding her research into spiral DRAGNs discovered through the citizen science project Galaxy Zoo, as well as the initial chance discoveries by herself and other groups prior. The use of Galaxy Zoo marked the first systemic search for spiral DRAGNs, yielding 3 new sources, doubling the number of previously known sources. Mao also details her current work on how VLBI is being used to probe into the centre of these galaxies in order to work out how SMBHs are able to generate such a massive amount of energy, despite the lack of an apparent merger.

\paragraph{}
Mao also hopes to use Radio Galaxy Zoo, the successor to Galaxy Zoo, to find additional spiral DRAGNs; the first results of the project have been presented, and are covered in more detail in section \ref{sec:RGZ}.

\subsubsection{Discovery of Spiral DRAGNs}

\paragraph{}
Galaxy Zoo utilises the Sloan Digital Sky Survey, an all sky survey aiming to map galaxies out to redshift $z=0.7$ \cite{lintott2008galaxy}; as a citizen science project, volunteers aim to classify galaxies visually. This project is important in it's own right, and has resulted in numerous discoveries, such as red spiral galaxies; another disruptive discovery \cite{masters2010galaxy}.

\paragraph{}
In order to find spiral DRAGNs, data from the Galaxy Zoo superclean sample was cross referenced with the Unified Radio Catalog. Galaxy Zoo was used since it provides a reliable, expansive database of spiral galaxy candidates. Out of the 65,492 galaxies surveyed, only 3 were potential matches, having bright, extended radio emission. One was later found to be a chance alignment of two galaxies, while another was a nearby radio source upon closer inspection; and the third was found to be radio galaxy J1649+26.

\subsubsection{Properties of \maogal}
\paragraph{}
An important point that Mao noted in the talk was that \maogal was significantly redder than normal, red spirals were previously discovered by Galaxy Zoo \cite{masters2010galaxy} and noted in previous discoveries; this implies that there was a process $\sim 500Myr$ ago that led to the cessation of star formation. In addition, bulge to disk ratios were calculated, to find an extremely large black hole, with a mass in the order of $5\times 10^8 M_\odot$, this is an extremely large SMBH for a spiral galaxy, and lends credence a merger event in the past.

\paragraph{}
\maogal is also a cluster member, with a total cluster mass of $10^{13}-10^{14}M_\odot$, which explain the optical halo, and suggests a dense environment.

\subsubsection{Potential Explanation of Phenomena}

\paragraph{}
A merger occurring $500Myr$ ago is one possible explanation, in particular an edge-on merger with a smaller galaxy was suggested \cite{naab2006surface} as this would be a fairly rare occurrence; it would explain why there are so few spiral DRAGNs in the low redshift universe.

\paragraph{}
Another explanation is that the galaxy simply "looks" spiral, and is in fact an elliptical galaxy which contains features common to spiral galaxies such as spiral arms. Spiral arms could form as a result of a recent merger but since arms require large amounts of gas and star formation to be visible these will decay quickly.

\paragraph{}
A third explanation is that spiral DRAGNs are in fact Seyfert galaxies in a dense environment, this is supported by the fact that spiral DRAGNs are typically found in a cluster environment, which typically have a denser IGM. As well as that, spiral DRAGNs seem to have a lower radio brightness than FR-II radio galaxies found in massive elliptical galaxies. In a dense environment radio lobes could perhaps be generated by Seyferts; if this were correct this would also agree with the hypothesis that spiral DRAGNs are much more prevalent in the early universe.

\subsubsection{The Heart of the DRAGN - Use of VLBI}

Very Long Baseline Interferometry utilises continent sized interferometric arrays\footnote{In the order $10^4 km$!} in order to maximise the angular resolution of an array at the expense of an incomplete synthetic aperture, in particular the Australian Long Baseline Array ($d=9853km$) was used. Initially VLBI was used to rule out if \maogal was undergoing a chance alignment with a background radio galaxy; this was successfully ruled out. VLBI was also used to determine whether the relativistic jets interacted with the Interstellar Medium, it has been determined that the jet appears normal, meaning that there is little interaction.

\paragraph{}
Interestingly galactic centre shows two large blobs of emission in the active galactic nuclei, which implies that the SMBH is off axis with the rotation of the galaxy. Use of even wider baselines using the next-generation European VLBI Networks e-EVN network to peer deeper into the heart of DRAGNs. e-EVN also has faster turnaround of data reduction, due to dedicated fibre lines.

\section{Discovery of Rare Double-Lobe Radio Galaxies Hosted in Spiral Galaxies \cite{singh2015discovery} - A Summary}\label{sec:discovery}
\paragraph{}
This paper has the most in common with Mao's work, and is the independent discovery of four additional spiral DRAGNs. As the current number of discovered spiral DRAGNs is fairly low the discovery of four is not only unprecedented, but also extremely helpful for the further categorisation and study of spiral DRAGNs.
\subsection{Introduction}
\paragraph{}
The methodology of \cite{singh2015discovery} is different to Mao's team in that they use a larger final catalogue, the Sloan Digital Sky Survey DR-7, which is cross referenced with the radio surveys FIRST and NVSS; this gave a total number of galaxies for analysis of 187,005.
\subsection{Observations}
\paragraph{}
The Sloan Digital Sky Survey is the source catalogue used in Galaxy Zoo, and is an optical survey using a dedicated 2.5 metre telescope, DR-7 was released in 2008; this is a fairly old survey considering that DR-12 has been available since 2014. The reason for this is that the catalogue used has been derived from \cite{meert2015catalogue} as it provides morphological classifications which are used to identify spirals. Using a more up-to-date survey would yield more discoveries, but would require work in classifying galaxies that are not present in DR-7; in addition galaxies would have to be assessed if they are present in radio surveys.

\paragraph{}
For radio data the FIRST survey was used, which uses the VLA in the 11.1km max baseline "B" configuration (figure \ref{fig:bconfig}) \cite{becker1995first}, giving it a maximum angular resolution ($\theta_{LAS}$) of $\approx$120 arc seconds at 1.4GHz\footnote{\url{https://science.nrao.edu/facilities/vla/docs/manuals/oss/performance/resolution}}, it should be noted that this survey is shared with the Radio Galaxy Zoo project. NVSS data (using the VLA in the "D" configuration\footnote{$d_{max}$ 1.03km, $\theta_{LAS_{1.4}} \approx$ 970 arc-seconds, $x,y$ range of figure is kept consistent with "B" configuration figure to indicate difference in uv distance and density of the synthetic aperture}, figure \ref{fig:dconfig}) is used in conjunction, as some faint galaxies may have some of their emissions "resolved out," or not collected by the wider synthetic aperture of the VLA in its "B" configuration.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../Images/Bconfig.png}
	\caption{VLA "B" configuration synthetic aperture, used by FIRST}
	\label{fig:bconfig}
\end{figure}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../Images/Dconfig.png}
	\caption{VLA "D" configuration synthetic aperture, used by NVSS}
	\label{fig:dconfig}
\end{figure}

\paragraph{}
It can be argued that the exclusive use of VLA interferometry data may result in the lack of detections of spiral DRAGNs, as the radio emission may still be "resolved out" due to gaps in the synthetic aperture; especially the inherent lack of sensitivity at larger angular scales due to the interferometry process. This can be alleviated by rotational synthesis, building up the aperture over time using the rotation of the earth. However this process is time consuming, imperfect and would not have been used in the NVSS and FIRST surveys for this reason. Potentially monolithic radio telescopes could also be used to further resolve large scale features.

\paragraph{}
NVSS data is used for MPc scale radio lobes, due to its increased resolving power at those length scales, otherwise FIRST data is used exclusively. The central component of the radio emission is matched to the centre of the spiral galaxy under the reasonable assumption that the galactic core is the source of the kiloparsec/megaparsec scale emission. Due to the decreased resolution of the radio data, there were alleged problems with the positional accuracy; but has been described as negligible.

\subsection{Results and Discussion}
\paragraph{}
After cross matching radio and optical data, 5,030 galaxies showed strong radio signatures, ruling out Seyferts by scanning for extended double-lobed radio sources, 4 sources were found\footnote{J1649+2635 was discovered by \cite{mao2015j1649+} at approximately the same time}. These candidates all take the form of FR-II galaxies. As optical spectrographic data was available from the DR-7 catalogue, spectral properties and star formation rates could be ascertained.

\paragraph{}
Star formation rates of the 4 newly discovered spiral DRAGNs were found to be:

\begin{table}[H]
\centering
\caption{Star formation rates}
\label{my-label}
\begin{tabular}{c|c}
\hline
\textit{Source} & \textit{SFR}         \\ 
\textit{Name}   & \textit{($M_\odot/yr$} \\ \hline\hline
J0836+0532      & $9.99^{+9.66}_{-3.76}$ \\ \hline
J1159+5820      & $2.89^{+2.78}_{-1.16}$ \\ \hline
J1352+3126      & $7.41^{+7.12}_{-2.99}$ \\ \hline
J1649+2635      & $1.67^{+1.60}_{-0.67}$ \\ 
\end{tabular}
\end{table}

\paragraph{}
This is lower than typical for gas rich spiral galaxies, and far below a starburst formation rate, the galaxies are also particularly red compared to typical spirals, matching Mao's observations.

\paragraph{}
Similar to Mao's work, the black hole mass was estimated using the bulge-luminosity relation from \cite{magorrian1998demography}, and were found to all be in the order of $10^8M_\odot$, consistent with Mao et. al's work; and indicative of a common trend between spiral DRAGNs of possessing an extremely large SMBH. However, only one galaxy is found in a cluster environment, which suggests that mergers and accretion are not the sole factor for the creation of a spiral DRAGN.

\paragraph{}
\cite{singh2015discovery} speculate that the formation of spiral DRAGNs may in fact be due to multiple factors, as opposed to a single reason. Most notably that optical spectroscopy finds the presence of emission lines consistent with that of a Seyfert galaxy; while Seyferts can display double-lobe structure, they are typically on significantly smaller scales than those observed in this survey. It was hypothesised that the combination of an extremely large SMBH and a minor planar merger could be the causes behind this anomalous behaviour, in agreement with Mao's work.

\paragraph{}
Observations of DRAGNs with disturbed disks imply a different scenario, that a large merger event has occurred, but by chance has not disturbed the spiral arms of the DRAGN. This is also backed up by the presence of bluer spiral arms in these galaxies; this implies very recent star formation, indicative of a starburst merger. Finally, \cite{singh2015discovery} note that while a cluster environment may be important for a spiral DRAGN, it may not be the only reason for them; thus is not cited as a primary factor in their formation.

\section{Radio Galaxy Zoo: Host Galaxies and Radio Morphologies For Large Surveys From Visual Inspection \cite{willett2016radio} - A Summary}\label{sec:RGZ}
\paragraph{}
Radio Galaxy Zoo is a web based citizen science project, designed to allow users to visually inspect and classify galaxies, using all-sky radio surveys. So far, 60,000 sources have been classified, with over 1 million classifications from users\footnote{Galaxies are classified by multiple users; with a consensus of 75\% being required to be added to the final catalogue. This was performed in Galaxy Zoo as well, where the superclean sample used by \cite{mao2015j1649+} required many users to make the same judgement.}; \cite{willett2016radio} compares the results from multiple classifications by the volunteers to be comparable in accuracy to visual inspection by an expert science team.

\paragraph{}
The primary purposes of this project are to:

\begin{itemize}
	\item Provide a catalogue of galaxies displaying radio emission.
	\item Use the resultant catalogue and data to train machine learning algorithms for automated detection in future catalogues.
	\item Establish roles and practises for larger future citizen science projects, such as those to be performed by the SKA.
\end{itemize}

\subsection{Introduction}
\paragraph{}
The paper cites large-scale and all-sky radio surveys as the most important method of exploring the structure and evolution of the universe; this is a fair assumption to make as radio emission is produced in all kinds of low and high-energy interactions, making it a versatile tool. 21cm line emission is used to detect cold molecular clouds, the sites of star formation; while UHF radio emission is detected through synchrotron and bremsstrahlung through thermal processes. Combining radio with optical or IR data for multi-spectral analysis is especially important for large scale, high energy phenomena such as radio galaxies.

\paragraph{}
The difficulties of multi-spectral observation are also discussed in some detail, positional matching of data is difficult for radio data; as radio telescopes are still a diffraction limited system and hence must adhere to the Rayleigh criterion:
\begin{equation}
\theta \propto \frac{\lambda}{d}
\end{equation}

\paragraph{}
where $\theta$ is the angular limit, $\lambda$ is the desired wavelength, and $d$ is the telescope diameter. While angular resolution can be drastically increased using interferometric arrays, radio emission can still be extremely far removed from the object driving them; for instance SMBH driving relativistic jets causing synchrotron emission in radio lobes, such as the case from spiral DRAGNs.

\paragraph{}
This removal means that visual inspection is still important. This still cannot be done with a reasonable degree of success algorithmically, for instance \cite{fan2015matching}'s system used in the ATLAS survey can correctly identify single sources; however the algorithm fails to discern between chance alignments of two or more sources and a singular source.

\paragraph{}
Additionally, the algorithm is trained to use realistic geometries to characterise sources, however this will not work for every source; a self-taught, machine learning algorithm would potentially be able to overcome these obstacles, but would first require data in which to be taught, one of the purposes of the Radio Galaxy Zoo project. 

\subsection{Data, Website \& Interface}
\paragraph{}
The main source of data for the first wave of Radio Galaxy Zoo is the FIRST $1.4\text{GHz}$ survey \cite{becker1995first}, which contains 174,821 extended resolved sources in the $21cm$ band, as well as IR data in the form of the WISE survey \cite{wright2010wide}, using the Spitzer telescope, in order to provide the highest resolution infrared data available for each source.

\paragraph{}
The main challenge of a citizen science project is to provide a simplistic interface for the analysis of data, allowing as much data as possible to be displayed without becoming confusing to the end user. Radio Galaxy Interface, hosted on the Zooniverse website, attempts to solve this problem by overlaying radio data with an infrared background (figure \ref{fig:rgz}). The user can then identify the galaxy visually with the radio layer overlaid, which is processed to only display intensities with $>3\sigma$. Using the $3\sigma$ rule-of-thumb rules out erroneous data that would be discounted by a professional, but potentially not by a user of the site.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../Images/RGZ}
	\caption{An example of the radio galaxy interface}
	\label{fig:rgz}
\end{figure}


\paragraph{}
Images are randomly selected and previous users classifications of the object are hidden in order to prevent bias, as a typical user could be swayed by the false consensus bias due to other users.

\paragraph{}
After reaching a consensus after 5 (for single radio source) or 20 (for multiple radio sources), the image is added to a database, and removed from the RGZ interface, in order to focus on other sources. The findings of the volunteers is deconvolved from the images, matching the emissions and sources to the WISE source catalogue and SDSS galaxy table. Other properties, such as redshift, distance and size are added from other assorted databases if known.

\subsection{Results and Discussion}
\subsubsection{Comparison to WISE}
\paragraph{}
The distribution of sources discovered by RGZ matches previous surveys, such as WISE, however there is a population of galaxies that are significantly redder, which matches the findings of Galaxy Zoo \cite{masters2010galaxy}; however this could be potentially due to higher than average dust content.

\subsubsection{HyMORS}
\paragraph{}
An important discovery already made by Radio Galaxy Zoo is the discovery of new Hybrid Radio Galaxies (Referred to as HyMORs\footnote{Hybrid MOrpholohy Radio Sources}), another unusual variation of radio galaxies in a similar vein to spiral DRAGNs. HyMORs have the interesting property of having both FR-I and FR-II, figure \ref{fig:hymor} shows the distinct asymmetry of the radio lobes, with especially the disconnect between the northern lobe and the galaxy, indicative of a FR-II source.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\linewidth]{../Images/hymor}
	\caption{An example of HyMOR emission, source  \cite{gawronski2006hybrid}}
	\label{fig:hymor}
\end{figure}

\paragraph{}
11 more HyMORs were discovered by the time of publication using Radio Galaxy Zoo, as is the case when increasing the population of a phenomena, this allows for a more accurate definition of the phenomena.

%Further detail HyMORS

\subsubsection{Bending Angles}
\paragraph{}
Another important result of the project is a catalogue of bent relativistic jets, which will help explain the processes behind this phenomena. The prevailing theory for the bending process is interaction with the IGM\footnote{Reffered to as the Intra-Cluster Medium (ICM) in \cite{willett2016radio}}, measuring the bending angle can be used to assess the conditions of the IGM in the vicinity of radio galaxies.

\paragraph{}
This could be tied into the work being done on spiral DRAGNs, in verifying theories behind their formation, and if DRAGNs are just Seyferts interaction with a denser than normal IGM, due to being in a cluster environment \cite{mao2015j1649+}.

\paragraph{}
Bending angle can be automatically measured in galaxies with a large angular size as the plane of the galaxy can be easily determined. Bending has been noted as more drastic closer to the centre of a cluster, which lends weight to the theory that it is due to a denser IGM.

\section{Conclusion}
\subsection{Mao et. al's Work}
\paragraph{}
Mao's work has resulted in the discovery of many new spiral DRAGNs, and the extension of the project using RGZ and the e-EVN network will perhaps yield important knowledge about spiral DRAGNs, and this complex, largely unknown subset of galactic evolution.

\paragraph{}
Mao and her team hypothesise that the denser IGM plays an important role in the establishment of larger than normal radio lobes, believing that a cluster environment helps foster the formation of a spiral DRAGN, a planar merger, another likelihood in a cluster would also increase the in-fall mass to the threshold required to produce radio lobes.

\subsection{Singh et. al's Work}
\paragraph{}
Singh's team is largely in agreement with Mao's team's findings, however they downplay the importance of the cluster environment as a reason for a spiral DRAGNs formation, instead hypothesizing that DRAGNs are either radio loud Seyfert galaxies, or the result of an elliptical-massive spiral merger.

\paragraph{}
Both Mao and Singh use interferometric survey data, that might have issues with extremely large-scale sensitivity, that may hamper the discovery of fainter DRAGNs, such as ones discovered by \cite{mulcahy2016discovery}, which used the monolithic Giant Meterwave Telescope.
\subsection{Willet et. al's Work}
\paragraph{}
While still underway, Radio Galaxy Zoo is already providing interesting discoveries, bending angles could be used to determine the importance of the IGM in the formation of spiral DRAGNs, while HYMORs are an interesting new phenomena that could be seen to challenge our current preconception of active galactic nuclei.

\paragraph{}
Radio Galaxy Zoo already looks to be another important citizen science project, a rapidly expanding field due to the increasing availability of the internet. 

\end{multicols}

\newpage
\pagenumbering{roman}
\section{Appendix}
\subsection{List of Discovered Spiral DRAGNs}\label{listDRAGN}

\scriptsize
\begin{longtable}{c|c|c|c|c|c|c|c}

\multicolumn{1}{c|}{\textit{Source}} & \multicolumn{1}{c|}{\textit{$z$}} & \multicolumn{1}{c|}{\textit{Scale}} & \multicolumn{1}{c|}{\textit{Size}} & \multicolumn{1}{c|}{\textit{$L_{1.4GHz}$}} & \multicolumn{1}{c|}{\textit{FR}} & \multicolumn{1}{c|}{\textit{Environment}} & \multicolumn{1}{c}{\textit{Reference}} \\ 
\multicolumn{1}{c|}{\textit{Name}} & \multicolumn{1}{c|}{\textit{}} & \multicolumn{1}{c|}{\textit{(kpc arcsec$^{-1}$)}} & \multicolumn{1}{c|}{\textit{(kpc)}} & \multicolumn{1}{c|}{\textit{(WHz$^{-1}$)}} & \multicolumn{1}{c|}{\textit{Type}} & \multicolumn{1}{c|}{\textit{}} & \multicolumn{1}{c}{\textit{}} \\ \hline \hline
J0836+0532 & 0.099 & 1.756 & 420 & $1.53 � 10^{24}$ & FR-II & Field & \cite{singh2015discovery} \\ \hline
J1159+5820 & 0.054 & 1.037 & 392 & $2.26 � 10^{24}$ & FR-II & Field & \cite{singh2015discovery} \\ \hline
J1352+3126 & 0.045 & 0.877 & 335 & $2.26 � 10^{25}$ & FR-II & Field & \cite{singh2015discovery} \\ \hline
J1649+2635 & 0.055 & 1.046 & 86 & $1.07 � 10^{24}$ & FR-II & Group/BCG & \cite{mao2015j1649+} \\ \hline
0313-192 & 0.067 & 1.268 & 200 & $1.0 � 10^{24}$ & FR-I & Abell 428 & \cite{ledlow1998unusual} \\ \hline
Speca & 0.138 & 2.411 & 1000 & $7.0 � 10^{24}$ & FR-II & BCG & \cite{hota2011discovery} \\ \hline
J2345-0449 & 0.076 & 1.423 & 1600 & $2.5 � 10^{24}$ & FR-II & Field & \cite{bagchi2014megaparsec} \\ 
\end{longtable}


\normalsize

\subsection{Synthetic Aperture Generation \& Discussion}

The figures \ref{fig:bconfig} \& \ref{fig:dconfig} were generated by using data from a 3rd year lab project on radio interferometry, using data from \cite{fey1994high}. This data was flagged and processed using MIRIAD for that project, and included in this paper to show the difference in synthetic aperture size between configurations, with a $u,v$ distance significantly lower, and the same number of antennae and baselines, the configuration is significantly denser, reducing the likelihood that visibilities will be "resolved out" at the expense of a lower $\theta_{LAS}$.

\section{Lists \& References}
\listoffigures
\listoftables
\bibliography{paperbib}
\bibliographystyle{plain}
\end{document}
