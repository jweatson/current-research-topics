#Current Research Topics in Physics
***Author - Joseph Eatson***

***Semester 1&2, 4th year, 2016-2017***

##What is the Purpose of This?

This repository acts as a backup for the essays and presentations produced for the sake of this module.

The intent of this module is to produce two literature reviews, based on a topic of a presentation hosted by the physics department, such as the Bolton lecture, as well as a poster on another topic.

The papers will be written in LaTeX, and the poster will most likely be composited using Adobe InDesign.

In addition to the papers, notes on the papers read and notes written during the talk will also be published.

##What Talks Are Being Written About?
###Literature Review
- Semester 1 - *Here be DRAGNs, Dr. Minnie Mao, University of Manchester, Jodrell Bank*
- Semester 2 - **Unknown**

###Poster
- Semester 2 - **Unknown**